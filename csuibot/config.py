from os import environ

gaz = '330679569:AAGEmBsFUKEFt0n2N5z12Z_sImUxPDRWXmA'

APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', gaz)
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://young-plains-94583.herokuapp.com/bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'https://young-plains-94583.herokuapp.com/')
